# Cambrionix Java API # 
Simple API allowing to interface with the Cambrionix charger.

Not all features of the charger are supported yet
To get it running, clone the repo and do 

``` console
mvn test
```

For the tests you need to have a device connected to port 1 of the Cambrionix since it tries to turn it on and off. Also the device needs to be loaded at `/dev/ttyUSB1`. In future versions, the tests should be parametrized with the path to the device :).

If the tests won't run or you don't have a device, simply append `-DskipTests` so the goal will finish without running the tests, e.g.

```console
mvn package -DskipTests # for creating the jar
mvn install -DskipTests # for making it available for other maven projects
```

## Usage ##
Simply create a Cambrionix-Object by passing the device. Each port is  represented by an instance of the `CambrionixPort` class. Optionally, an auto poller (separate thread) can be started that automatically updates the port state.

For an example, see [Example.java](src/master/src/test/java/cambrionix/example/Example.java)

    
    