package cambrionix.example;

import cambrionix.api.Cambrionix;
import cambrionix.api.CambrionixException;
import cambrionix.api.CambrionixPort;

public class Example {

	public static void main(String[] args) throws CambrionixException {
		// create the
		Cambrionix cambrionix = new Cambrionix("/dev/ttyUSB1");

		// get port 1
		CambrionixPort port = cambrionix.getPort(1);

		// retrieve some values
		System.out.println(port.getDrawnCurrent());
		System.out.println(port.getEnergy());
		System.out.println(port.getUptime());

		// turn it off
		port.turnOff();

		// start the state poller. It updates the state of all ports
		// in a given interval.
		cambrionix.startPoller();

		// shut it down
		cambrionix.close();

	}
}
