package cambrionix.api;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CambrionixTest {

	private Cambrionix camb;

	@Before
	public void setUp() throws CambrionixException {
		this.camb = new Cambrionix("/dev/ttyUSB1");
	}

	@After
	public void tearDown() throws CambrionixException {
		this.camb.close();
		this.camb = null;
	}

	@Test
	public void testUpdatePorts() throws CambrionixException {
		this.camb.updatePorts();
	}

	@Test
	public void testGetProfiles() throws CambrionixException {
		Assert.assertEquals(5, this.camb.getAllProfiles().size());
	}

	public void testDisableEnableProfile() throws CambrionixException {
		this.camb.enableProfile("4");
		Assert.assertEquals("enabled", this.camb.getAllProfiles().get("4"));
		this.camb.disableAllProfiles();
		Assert.assertEquals("disabled", this.camb.getAllProfiles().get("4"));
		this.camb.enableProfile("4");
	}
}
