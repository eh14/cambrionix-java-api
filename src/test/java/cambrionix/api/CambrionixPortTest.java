package cambrionix.api;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;

public class CambrionixPortTest {

	@Test
	public void testCodeMap() {
		for (PortMode portMode : PortMode.values()) {
			Assert.assertEquals(portMode,
					PortMode.mapfromCode(portMode.getCode()));
		}
	}

	@Test
	public void testGetPortIdAsString() throws CambrionixException {
		CambrionixPort port = new CambrionixPort(null, 123, Arrays.asList(
				"123", "123", "1", "1", "1", "1", "1"));
		Assert.assertEquals("123", port.getIdAsString());
	}

	@Test
	public void testTurnOnOff() throws CambrionixException,
			InterruptedException {
		Cambrionix camb = new Cambrionix("/dev/ttyUSB1");
		camb.startPoller();
		CambrionixPort port = camb.getPort(1);

		port.turnOn();
		// wait ten seconds while polling for the state update. If
		// it's not happend by then, fail the test
		boolean testOk = false;
		for (int i = 0; i < 10; ++i) {
			if (port.isOn()) {
				testOk = true;
				break;
			}
			Thread.sleep(1000);
		}
		if (!testOk) {
			Assert.fail("failed to turn on port");
		}

		port.turnOff();

		testOk = false;
		for (int i = 0; i < 10; ++i) {
			if (port.isOff()) {
				testOk = true;
				break;
			}
			Thread.sleep(1000);
		}
		if (!testOk) {
			Assert.fail("failed to turn on port");
		}

		camb.close();
	}
}
