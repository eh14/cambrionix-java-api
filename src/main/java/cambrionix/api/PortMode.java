package cambrionix.api;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 */
public enum PortMode {

	OFF("O"),

	SYNC("S"),

	BIASED("B"),

	IDLE("I"),

	PROFILING("P"),

	CHARGING("C"),

	FINISHED("F"),

	ATTACHED("A"),

	DETACHED("D"),

	THEFT("T"),

	ERRORS("E"),

	REBOOTED("R");

	private final String code;

	private static final Map<String, PortMode> codeMap;

	static {
		codeMap = new HashMap<String, PortMode>();

		for (PortMode portMode : PortMode.values()) {
			codeMap.put(portMode.code, portMode);
		}
	}

	private PortMode(final String code) {
		this.code = code;
	}

	public String getCode() {
		return this.code;
	}

	public static PortMode mapfromCode(final String code) {
		if (!codeMap.containsKey(code)) {
			throw new IllegalArgumentException("Code is not a valid Port Mode");
		}
		return codeMap.get(code);
	}

}
