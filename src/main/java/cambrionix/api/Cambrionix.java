package cambrionix.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

public class Cambrionix {
	private SerialPort serialPort;

	private Map<Integer, CambrionixPort> ports;

	private final int READ_TIMEOUT = 1000;

	private final int POLL_INTERVAL = 1000;

	private CambrionixPoller poller;

	private static final String COMMAND_SUFFIX = "\r\n";

	public Cambrionix(final String device) throws CambrionixException {
		this.serialPort = new SerialPort("/dev/ttyUSB0");

		this.ports = new HashMap<Integer, CambrionixPort>();

		try {
			if (!this.serialPort.openPort()) {
				throw new CambrionixException(
						"Could not connect to the Cambrionix at device "
								+ device);
			}

			if (!this.serialPort.setParams(SerialPort.BAUDRATE_115200, 8, 1,
					SerialPort.PARITY_NONE)) {
				throw new CambrionixException(
						"Could not set connection parameters (baudrate etc.)");
			}
		} catch (SerialPortException e) {
			throw new CambrionixException(
					"Could not set connection parameters (baudrate etc)", e);
		}

		this.updatePorts();

	}

	public void startPoller() {
		if (this.poller != null) {
			return;
		}

		this.poller = new CambrionixPoller(this, POLL_INTERVAL);
		this.poller.start();
	}

	public void stopPoller(boolean wait) throws CambrionixException {
		try {
			if (this.poller != null) {
				this.poller.stopPoll();

				if (wait) {
					// wait at least until the read timeout is done, then
					// close the serial port. Otherwise we'll get exceptions on
					// closing a serial port while a thread still blocks for
					// writing
					this.poller.join(READ_TIMEOUT);
				}
				this.poller = null;
			}
		} catch (InterruptedException e) {
			throw new CambrionixException(
					"Exception stopping the state poller", e);
		}
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		this.close();
	}

	public void close() throws CambrionixException {
		try {
			this.stopPoller(true);
			this.serialPort.closePort();
		} catch (SerialPortException e) {
			throw new CambrionixException("Error closing the serial port", e);
		}
	}

	synchronized private <T> T sendCommand(final String command,
			final SerialResponseHandler<T> responseHandler)
			throws CambrionixException {
		String sendCommand = command;
		if (!command.endsWith(COMMAND_SUFFIX)) {
			sendCommand += COMMAND_SUFFIX;
		}
		try {
			if (!this.serialPort.writeString(sendCommand)) {
				throw new CambrionixException("Could not send command "
						+ command);
			}

			StringBuffer buffer = new StringBuffer();
			while (buffer.length() < 2
					|| !buffer.substring(buffer.length() - 2).equals(">>")) {
				buffer.append(this.serialPort.readString(1, READ_TIMEOUT));
			}
			String response = buffer.substring(0, buffer.length() - 2);
			if (response == null) {
				throw new CambrionixException(
						"Sending failed. Got a null response");
			}
			response = response.trim();
			if (response.startsWith(command)) {
				response = response.substring(command.length()).trim();
			}
			return responseHandler.handleResponse(response);

		} catch (SerialPortException e) {
			throw new CambrionixException("Could not send command" + command, e);
		} catch (SerialPortTimeoutException e) {
			throw new CambrionixException(
					"Timeout while reading command response", e);
		}

	}

	public String sendCommandForRaw(final String command)
			throws CambrionixException {
		return this.sendCommand(command,
				SerialResponseHandler.RAW_RESPONSE_HANDLER);
	}

	public Map<String, String> sendCommandForMap(final String command)
			throws CambrionixException {
		return this.sendCommand(command,
				SerialResponseHandler.MAP_RESPONSE_HANDLER);
	}

	public List<List<String>> sendCommandForTable(final String command)
			throws CambrionixException {
		return this.sendCommand(command,
				SerialResponseHandler.TABLE_RESPONSE_HANDLER);
	}

	public void updatePorts() throws CambrionixException {
		List<List<String>> stateList = this.sendCommandForTable("state");

		try {
			for (List<String> portState : stateList) {
				int portId = new Integer(Integer.parseInt(portState.get(0)));

				if (this.ports.containsKey(portId)) {
					this.ports.get(portId).update(portState);
				} else {
					this.ports.put(portId, new CambrionixPort(this, portId,
							portState));
				}
			}
		} catch (IndexOutOfBoundsException e) {
			throw new CambrionixException(
					"Invalid State format somewhere while parsing the state.",
					e);
		} catch (NumberFormatException e) {
			throw new CambrionixException(
					"Invalid number format somewhere while parsing the state response",
					e);
		}
	}

	public CambrionixPort getPort(int portId) throws CambrionixException {
		if (!this.ports.containsKey(portId)) {
			throw new CambrionixException("Cannot find port with id " + portId);
		}
		return this.ports.get(portId);
	}

	public Map<String, String> getAllProfiles() throws CambrionixException {
		return this.sendCommand("list_profiles",
				new SerialResponseHandler.MapResponseHandler(","));
	}

	public void disableAllProfiles() throws CambrionixException {
		Map<String, String> allProfiles = this.getAllProfiles();

		for (String profileId : allProfiles.keySet()) {
			this.sendCommandForRaw("en_profile " + profileId + " 0");
		}

	}

	public void enableProfile(String profileId) throws CambrionixException {
		Map<String, String> allProfiles = this.getAllProfiles();

		if (!allProfiles.containsKey(profileId)) {
			throw new CambrionixException("Unknown profile " + profileId);
		}

		this.sendCommandForRaw("en_profile " + profileId + " 1");
	}

	private static class CambrionixPoller extends Thread {
		private Cambrionix cambrionix;

		private boolean stop = false;

		private int interval;

		public CambrionixPoller(Cambrionix cambrionix, final int interval) {
			super("CambrionixPoller");

			this.cambrionix = cambrionix;
			this.interval = interval;
		}

		public void stopPoll() {
			this.stop = true;
		}

		public void run() {
			while (!this.stop) {
				try {
					this.cambrionix.updatePorts();
					Thread.sleep(this.interval);
				} catch (CambrionixException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					break;
				}
			}
		}
	}
}
