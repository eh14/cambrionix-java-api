package cambrionix.api;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CambrionixPort {

	private final Set<PortMode> flags = new HashSet<PortMode>();

	private final int portId;

	/**
	 * Currently drawn current in mA
	 */
	private int drawnCurrent;

	/**
	 * Time since connection in seconds
	 */
	private int uptime;

	/**
	 * Energy used since start in Wh
	 */
	private float energy;

	/**
	 * Back-reference to the gambrionix, so the port can send commands to the
	 * serial interface.
	 */
	private final Cambrionix host;

	public CambrionixPort(final Cambrionix host, final int portId,
			List<String> state) throws CambrionixException {
		this.portId = portId;
		this.host = host;
		this.update(state);
	}

	public void update(List<String> state) throws CambrionixException {

		if (state.size() != 7) {
			throw new CambrionixException(
					"Port state is expected to have 7 values. Got "
							+ state.size());
		}

		try {
			this.drawnCurrent = Integer.parseInt(state.get(1));
			this.flags.clear();

			String flagString = state.get(2);
			for (PortMode portMode : PortMode.values()) {
				if (flagString.contains(portMode.getCode())) {
					this.flags.add(portMode);
				}
			}
			this.uptime = Integer.parseInt(state.get(4));
			this.energy = Float.parseFloat(state.get(6));

		} catch (NumberFormatException e) {
			throw new CambrionixException(
					"Error updating the PortState due to formatting issues", e);
		}
	}

	String getIdAsString() {
		return String.valueOf(this.portId);
	}

	public void turnOn() throws CambrionixException {
		String response = this.host.sendCommandForRaw("mode c "
				+ this.getIdAsString() + " 1");
		if (response.length() > 0) {
			throw new CambrionixException("Error setting mode for port, got "
					+ response);
		}
	}

	public void turnOff() throws CambrionixException {
		String response = this.host.sendCommandForRaw("mode o "
				+ this.getIdAsString());

		if (response.length() > 0) {
			throw new CambrionixException("Error setting mode for port, got "
					+ response);
		}
	}

	public boolean isOn() {
		return this.flags.contains(PortMode.CHARGING)
				|| this.flags.contains(PortMode.PROFILING)
				|| this.flags.contains(PortMode.FINISHED);
	}

	public boolean isOff() {
		return this.flags.contains(PortMode.OFF);
	}

	public int getPortId() {
		return portId;
	}

	public int getDrawnCurrent() {
		return drawnCurrent;
	}

	public int getUptime() {
		return uptime;
	}

	public float getEnergy() {
		return energy;
	}

}
