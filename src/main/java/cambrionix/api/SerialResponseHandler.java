package cambrionix.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

abstract public class SerialResponseHandler<T> {

	public static final TableResponseHandler TABLE_RESPONSE_HANDLER = new TableResponseHandler();
	public static final MapResponseHandler MAP_RESPONSE_HANDLER = new MapResponseHandler(
			":");
	public static final RawResponseHandler RAW_RESPONSE_HANDLER = new RawResponseHandler();

	abstract public T handleResponse(final String response);

	/**
	 * Expects a response of the form
	 * 
	 * a, b, c
	 * 
	 * d, e, f
	 * 
	 * and returns a nested list of those values.
	 * 
	 */
	public static class TableResponseHandler extends
			SerialResponseHandler<List<List<String>>> {

		@Override
		public List<List<String>> handleResponse(final String response) {
			List<List<String>> tableResponse = new ArrayList<List<String>>();
			String[] lines = response.split("[\r\n]+");
			for (String line : lines) {
				line = line.trim();
				if (line.isEmpty()) {
					continue;
				}

				tableResponse.add(Arrays.asList(line.split("\\s*,\\s*")));
			}

			return tableResponse;
		}
	}

	/**
	 * Expects a response of the form:
	 * 
	 * a:1\nb:2
	 * 
	 * and returns a map of those values
	 * 
	 */
	public static class MapResponseHandler extends
			SerialResponseHandler<Map<String, String>> {

		private final String keyValueSeparator;

		public MapResponseHandler(final String keyValueSeparator) {
			this.keyValueSeparator = keyValueSeparator;
		}

		@Override
		public Map<String, String> handleResponse(final String response) {
			Map<String, String> mapResponse = new Hashtable<String, String>();
			for (String line : response.split("[\r\n]+")) {
				line = line.trim();
				if (line.isEmpty()) {
					continue;
				}
				String[] split = line.split(this.keyValueSeparator);

				// silently ignore misformed lines
				if (split.length != 2) {
					continue;
				}
				mapResponse.put(split[0].trim(), split[1].trim());
			}
			return mapResponse;
		}
	}

	/**
	 * Default response handler that does not transform the response in any way.
	 * Just returns the response as a string.
	 * 
	 */
	public static class RawResponseHandler extends
			SerialResponseHandler<String> {

		@Override
		public String handleResponse(final String response) {
			return response;
		}

	}

}
