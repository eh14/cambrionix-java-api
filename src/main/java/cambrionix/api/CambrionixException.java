package cambrionix.api;
public class CambrionixException extends Exception {

	private static final long serialVersionUID = -7761143976161752349L;

	public CambrionixException(String message) {
		super(message);
	}

	public CambrionixException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
}
