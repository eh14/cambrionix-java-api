package cambrionix.api;
import jssc.SerialPort;

public class PythonTest {

	public static void main(String[] args) throws Exception {
		SerialPort port = new SerialPort("/dev/ttyUSB1");

		System.out.println("opened" + port.openPort());
		System.out.println("setparams"
				+ port.setParams(SerialPort.BAUDRATE_115200, 8, 1,
						SerialPort.PARITY_NONE));
		System.out.println("send" + port.writeString("state\r\n"));

		while (true) {
			String read = port.readString(2, 100);
			System.out.print(read);
			if (read.equals(">>")) {
				break;
			}
		}
		port.closePort();
	}
}
